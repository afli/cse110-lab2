package edu.ucsd.cs110s.temperature; 
public class Celsius extends Temperature 
{ 
	public Celsius(float t) 
	{ 
			super(t); 
	}
 

	public String toString() 
	{ 
	// TODO: Complete this method 
	return String.valueOf(this.getValue()); 
	}


	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		return this;
	}


	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		float converted = (this.getValue()*((float)9/(float)5))+32;
		Temperature returnTemp = new Fahrenheit(converted);
		return returnTemp;
	} 
}
