package edu.ucsd.cs110s.temperature; 
public class Fahrenheit extends Temperature 
{ 
		public Fahrenheit(float t) 
		{ 
				super(t); 
		} 
		public String toString() 
		{ 
			// TODO: Complete this method 
			return String.valueOf(this.getValue()); 
		}
		@Override
		public Temperature toCelsius() {
			// TODO Auto-generated method stub
			float convert = (float)(this.getValue()-32)*(((float)5/(float)9));
			Temperature returnTemp = new Celsius(convert);
			return returnTemp;
		}
		@Override
		public Temperature toFahrenheit() {
			// TODO Auto-generated method stub
			return this;
		}
}
